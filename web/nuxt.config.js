export default {
  mode: 'spa',
  /*
   ** Headers of the page
   */
  head: {
    title: "GPS Tracker / Bachelor's thesis",
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      {
        hid: 'description',
        name: 'description',
        content: process.env.npm_package_description || ''
      }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/app/favicon.ico' },
      {
        rel: 'stylesheet',
        href: 'https://api.mapbox.com/mapbox-gl-js/v1.8.1/mapbox-gl.css'
      }
    ]
  },
  /*
   ** Customize the progress-bar
   */
  loading: false,
  /*
   ** Global CSS
   */
  css: ['assets/styles/main.scss'],
  /*
   ** Plugins to load before mounting the App
   */
  plugins: [],
  /*
   ** Nuxt.js dev-modules
   */
  buildModules: [
    '@nuxtjs/eslint-module',
    '@nuxtjs/tailwindcss',
    '@nuxtjs/color-mode',
    '@nuxtjs/moment'
  ],
  /*
   ** Nuxt.js modules
   */
  modules: ['@nuxtjs/axios', '@nuxtjs/dotenv', 'nuxt-webfontloader'],
  /*
   ** Webfontload configuration
   */
  webfontloader: {
    google: {
      families: ['Roboto:300,400,500,700,900']
    }
  },
  /*
   ** Moment configuration
   */
  moment: {
    defaultLocale: 'en',
    defaultTimezone: 'Europe/Ljubljana'
  },
  /*
   ** Axios module configuration
   ** See https://axios.nuxtjs.org/options
   */
  axios: {},
  /*
   ** Build configuration
   */
  build: {
    /*
     ** You can extend webpack config here
     */
    extend(config, ctx) {
      if (ctx.isDev) {
        config.devtool = ctx.isClient ? 'source-map' : 'inline-source-map'
      }
    }
  },
  /*
   ** Router configuration
   */
  router: {
    base: '/app/'
  },
  /*
   ** Runtime configuration
   */
  publicRuntimeConfig: {
    axios: {
      browserBaseURL: process.env.API_URL || 'https://thesis.bratec.si'
    },
    mapbox: {
      accessToken:
        'pk.eyJ1Ijoia2xlbWVuYnJhdGVjIiwiYSI6ImNrOTc1NjA3aDA3ZTEzbHBlNGpueWozcHAifQ.M6xOeNXzVmBNM1ADcEk73Q',
      mapStyle: 'mapbox://styles/mapbox/light-v10'
    }
  }
}
