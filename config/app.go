package config

import "github.com/tomazk/envcfg"

var App AppStruct

type AppStruct struct {
	PortHTTP int `envcfg:"PORT_HTTP"`
	PortCoAP int `envcfg:"PORT_COAP"`

	CertificatePath string `envcfg:"CERT_PATH"`
	KeyPath         string `envcfg:"KEY_PATH"`
	CaPath          string `envcfg:"CA_PATH"`
}

func init() {
	var err error

	App = AppStruct{
		PortHTTP: 8080,
		PortCoAP: 5683,

		CertificatePath: "ca/certs/home.bratec.si.cert.pem",
		KeyPath:         "ca/private/home.bratec.si.key.pem",
		CaPath:          "ca/certs/ca.cert.pem",
	}

	err = envcfg.Unmarshal(&App)

	if err != nil {
		panic(err)
	}
}
