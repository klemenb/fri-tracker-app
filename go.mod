module bratec.si/fri-tracker

go 1.14

require (
	github.com/dgraph-io/badger/v2 v2.2007.2
	github.com/gorilla/mux v1.8.0
	github.com/pion/dtls/v2 v2.0.2
	github.com/plgd-dev/go-coap/v2 v2.0.4
	github.com/tomazk/envcfg v0.0.0-20170619155318-23e3618f1e33
	golang.org/x/sys v0.0.0-20200831180312-196b9ba8737a // indirect
)
