#!/bin/bash

CERT_CNS="home.bratec.si"

for CERT_CN in ${CERT_CNS}
do
    # Generate client private key
    openssl ecparam -name prime256v1 -genkey -out private/${CERT_CN}.key.pem

    chmod 400 private/${CERT_CN}.key.pem

    # Generate server CSR
    openssl req -config openssl.cnf \
        -key private/${CERT_CN}.key.pem \
        -new -sha256 -out csr/${CERT_CN}.csr.pem \
        -subj "/C=SI/ST=Slovenia/O=Demo/CN=$CERT_CN"

    # Sign the server CSR
    openssl ca -batch -config openssl.cnf \
        -extensions server_cert -days 7300 -notext -md sha256 \
        -in csr/${CERT_CN}.csr.pem \
        -out certs/${CERT_CN}.cert.pem \

    # Verify server cert
    openssl verify -CAfile certs/ca.cert.pem \
        certs/${CERT_CN}.cert.pem

    if [[ ! $? -eq 0 ]]; then
        exit
    fi
done
