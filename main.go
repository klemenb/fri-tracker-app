package main

import (
	"fmt"
	"log"
	"os"
	"os/signal"
	"syscall"

	"bratec.si/fri-tracker/config"
	"bratec.si/fri-tracker/services/coap"
	"bratec.si/fri-tracker/services/http"
	"bratec.si/fri-tracker/services/storage"
)

func main() {
	var err error

	httpAddr := fmt.Sprintf(":%d", config.App.PortHTTP)
	coapAddr := fmt.Sprintf(":%d", config.App.PortCoAP)

	// Initialize storage
	badgerStorageService := storage.NewBadgerService()

	err = badgerStorageService.Init()

	if err != nil {
		log.Fatalf("Storage error: %s\n", err.Error())
	}

	defer badgerStorageService.Close()

	// Initialize HTTP server
	go func() {
		log.Printf("HTTP server listening on %s ...\n", httpAddr)

		httpServer := http.NewServer(httpAddr, badgerStorageService)
		err := httpServer.ListenAndServe()

		if err != nil {
			log.Fatal(err)
		}
	}()

	// Initialize CoAP server
	go func() {
		log.Printf("CoAP server listening on %s ...\n", coapAddr)

		err := coap.ListenAndServe(coapAddr, config.App.CertificatePath, config.App.KeyPath,
			config.App.CaPath, badgerStorageService)

		if err != nil {
			log.Fatal(err)
		}
	}()

	sigtermChan := make(chan os.Signal, 1)
	stop := make(chan bool)

	signal.Notify(sigtermChan, os.Interrupt, syscall.SIGTERM)

	go func() {
		<-sigtermChan
		stop <- true
	}()

	<-stop
}
