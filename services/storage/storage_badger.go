package storage

import (
	"encoding/json"
	"strconv"
	"time"

	"github.com/dgraph-io/badger/v2"
)

type BadgerService struct {
	DB *badger.DB
}

func NewBadgerService() *BadgerService {
	return &BadgerService{}
}

func (b *BadgerService) Init() (err error) {
	b.DB, err = badger.Open(
		badger.DefaultOptions("./badger/db").WithTruncate(true),
	)

	if err != nil {
		return
	}

	b.seedDatabase()

	return
}

func (b *BadgerService) Close() {
	b.DB.Close()
}

func (b *BadgerService) GetDevices() (devices []Device, err error) {
	devices = []Device{}

	err = b.DB.View(func(txn *badger.Txn) (err error) {
		it := txn.NewIterator(badger.DefaultIteratorOptions)
		defer it.Close()

		prefix := []byte("device")

		for it.Seek(prefix); it.ValidForPrefix(prefix); it.Next() {
			item := it.Item()

			err := item.Value(func(v []byte) error {
				device := Device{}
				_ = json.Unmarshal(v, &device)
				devices = append(devices, device)
				return nil
			})

			if err != nil {
				return err
			}
		}

		return nil
	})

	if err != nil {
		return
	}

	return
}

func (b *BadgerService) GetLocations(imei string) (locations []Location, err error) {
	locations = []Location{}

	err = b.DB.View(func(txn *badger.Txn) error {
		it := txn.NewIterator(badger.DefaultIteratorOptions)
		defer it.Close()

		prefix := []byte("location:" + imei)

		for it.Seek(prefix); it.ValidForPrefix(prefix); it.Next() {
			item := it.Item()

			err := item.Value(func(v []byte) error {
				location := Location{}
				_ = json.Unmarshal(v, &location)
				locations = append(locations, location)
				return nil
			})

			if err != nil {
				return err
			}
		}

		return nil
	})

	if err != nil {
		return
	}

	return
}

func (b *BadgerService) AddDeviceLocation(imei string, latitude float64, longitude float64, altitude float64, timestamp int64, ttl time.Duration) (err error) {
	packedLocation, _ := json.Marshal(&Location{
		Latitude:  latitude,
		Longitude: longitude,
		Altitude:  altitude,
		Timestamp: timestamp,
	})

	key := "location:" + imei + ":" + strconv.FormatInt(timestamp, 10)

	err = b.DB.Update(func(txn *badger.Txn) error {
		e := badger.NewEntry([]byte(key), packedLocation)

		if ttl != 0 {
			e = e.WithTTL(ttl)
		}

		err := txn.SetEntry(e)
		return err
	})

	return
}

func (b *BadgerService) seedDatabase() (err error) {
	devices := []Device{
		{
			Name: "nRF9160 DK",
			IMEI: "352656100219081",
		},
		{
			Name: "Thingy:91",
			IMEI: "352656101399148",
		},
	}

	for _, device := range devices {
		err = b.DB.Update(func(txn *badger.Txn) error {
			_jsonBytes, _ := json.Marshal(&device)

			err := txn.Set([]byte("devices:"+device.IMEI), _jsonBytes)
			return err
		})

		if err != nil {
			return err
		}

		currentLocations, _ := b.GetLocations(device.IMEI)

		if len(currentLocations) != 0 {
			continue
		}

		b.AddDeviceLocation(device.IMEI, 46.0499, 14.4675, 300, time.Now().Unix(), 0)
	}

	return
}
