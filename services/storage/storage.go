package storage

import "time"

type Location struct {
	Latitude  float64 `json:"lat"`
	Longitude float64 `json:"lng"`
	Altitude  float64 `json:"alt"`
	Timestamp int64   `json:"timestamp"`
}

type Device struct {
	Name string `json:"name"`
	IMEI string `json:"imei"`
}

type Service interface {
	Init() error
	Close()

	GetDevices() ([]Device, error)
	GetLocations(imei string) ([]Location, error)

	AddDeviceLocation(imei string, latitude float64, longitude float64, altitude float64, timestamp int64, ttl time.Duration) error
}
