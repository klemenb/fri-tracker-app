package http

import (
	"net/http"
	"time"

	"bratec.si/fri-tracker/services/storage"

	"github.com/gorilla/mux"
)

func NewServer(addr string, storageService storage.Service) *http.Server {
	r := mux.NewRouter()

	// Configure static file server under the "/app" path
	r.PathPrefix("/app/").Handler(
		http.StripPrefix("/app/", http.FileServer(http.Dir("./web/dist"))),
	)

	// Redirect "/app" to "/app/"
	r.Handle("/app", http.RedirectHandler("/app/", 301))

	// Configure API routes under the "/api" path
	r.Methods("GET", "OPTIONS").Path("/api/devices").Handler(NewGetDevicesHandler(storageService))
	r.Methods("GET", "OPTIONS").Path("/api/devices/{imei}/locations").Handler(NewGetLocationsHandler(storageService))

	// Add CORS middleware
	r.Use(mux.CORSMethodMiddleware(r))

	// Configure HTTP server timeouts
	httpServer := &http.Server{
		Handler:      r,
		Addr:         addr,
		WriteTimeout: 5 * time.Second,
		ReadTimeout:  5 * time.Second,
	}

	return httpServer
}
