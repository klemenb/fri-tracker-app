package http

import (
	"encoding/json"
	"log"
	"net/http"

	"bratec.si/fri-tracker/services/storage"
	"github.com/gorilla/mux"
)

type GetLocationsHandler struct {
	Storage storage.Service
}

// GetLocationsHandler returns locations for the device with the provided IMEI.
func NewGetLocationsHandler(s storage.Service) *GetLocationsHandler {
	return &GetLocationsHandler{s}
}

func (h *GetLocationsHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)

	devices, err := h.Storage.GetLocations(vars["imei"])

	if err != nil {
		log.Printf("storage error: %s", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	jsonResponse, _ := json.MarshalIndent(devices, "", "    ")

	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Access-Control-Allow-Origin", "*")

	_, _ = w.Write(jsonResponse)
}
