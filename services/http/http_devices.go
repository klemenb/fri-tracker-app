package http

import (
	"encoding/json"
	"log"
	"net/http"

	"bratec.si/fri-tracker/services/storage"
)

type GetDevicesHandler struct {
	Storage storage.Service
}

// GetDevicesHandler returns all devices from the database.
func NewGetDevicesHandler(s storage.Service) *GetDevicesHandler {
	return &GetDevicesHandler{s}
}

func (h *GetDevicesHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	devices, err := h.Storage.GetDevices()

	if err != nil {
		log.Printf("storage error: %s", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	jsonResponse, _ := json.MarshalIndent(devices, "", "    ")

	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Access-Control-Allow-Origin", "*")

	_, _ = w.Write(jsonResponse)
}
