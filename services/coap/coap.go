package coap

import (
	"context"
	"crypto/tls"
	"crypto/x509"
	"log"
	"time"

	"bratec.si/fri-tracker/services/storage"

	pionDTLS "github.com/pion/dtls/v2"
	"github.com/pion/dtls/v2/examples/util"
	"github.com/plgd-dev/go-coap/v2/dtls"
	"github.com/plgd-dev/go-coap/v2/mux"
	"github.com/plgd-dev/go-coap/v2/net"
)

func ListenAndServe(addr string, certPath string, keyPath string, caPath string, storageService storage.Service) error {
	router := mux.NewRouter()

	// Register the only available handler as the default handler
	router.DefaultHandle(NewDevicesHandler(storageService))

	// Read client certificates from PEM files
	serverCert, err := util.LoadKeyAndCertificate(
		keyPath,
		certPath,
	)

	if err != nil {
		log.Fatalln("Error loading server certificate and/or key: " + err.Error())
	}

	// Read CA certificate
	caCert, err := util.LoadCertificate(caPath)

	if err != nil {
		log.Fatalln("Error loading CA certificate: " + err.Error())
	}

	// Prepare certificate pool
	caCertPool := x509.NewCertPool()
	cert, _ := x509.ParseCertificate(caCert.Certificate[0])
	caCertPool.AddCert(cert)

	connectContextMaker := func() (context.Context, func()) {
		return context.WithTimeout(context.Background(), time.Minute)
	}

	// Prepare DTLS configuration
	dtlsConfig := pionDTLS.Config{
		ClientCAs:           caCertPool,
		ClientAuth:          pionDTLS.RequireAndVerifyClientCert,
		Certificates:        []tls.Certificate{*serverCert},
		ConnectContextMaker: connectContextMaker,
	}

	listener, err := net.NewDTLSListener("udp", addr, &dtlsConfig)

	if err != nil {
		return err
	}

	defer listener.Close()

	server := dtls.NewServer(dtls.WithKeepAlive(nil), dtls.WithMux(router))

	return server.Serve(listener)
}
