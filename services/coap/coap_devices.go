package coap

import (
	"io/ioutil"
	"log"
	"strconv"
	"strings"
	"time"

	"bratec.si/fri-tracker/services/storage"

	"github.com/plgd-dev/go-coap/v2/mux"
)

type DevicesHandler struct {
	Storage storage.Service
}

func NewDevicesHandler(s storage.Service) *DevicesHandler {
	return &DevicesHandler{s}
}

func (h *DevicesHandler) ServeCOAP(w mux.ResponseWriter, m *mux.Message) {
	resource, err := m.Options.Path()

	if err != nil {
		log.Printf("Error reading message path")
		return
	}

	bodyBytes, err := ioutil.ReadAll(m.Body)

	if err != nil {
		log.Printf("Error reading message body")
		return
	}

	body := string(bodyBytes)

	log.Printf("Received message for resource \"%s\": %s", resource, body)

	resourceParts := strings.Split(resource, "/")
	bodyParts := strings.Split(body, ",")

	imei := resourceParts[1]

	latitude, _ := strconv.ParseFloat(bodyParts[0], 64)
	longitude, _ := strconv.ParseFloat(bodyParts[1], 64)
	altitude, _ := strconv.ParseFloat(bodyParts[2], 64)
	timestamp := time.Now().Unix()

	err = h.Storage.AddDeviceLocation(imei, latitude, longitude, altitude, timestamp, time.Hour*24)

	if err != nil {
		log.Printf("Storage error: %s", err.Error())
	}
}
